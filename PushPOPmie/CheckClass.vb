﻿Public Class CheckClass
    'readData
    Public Function funtionRead(ByVal objName)
        Return My.Computer.FileSystem.ReadAllText(Application.StartupPath & "\" & objName & ".txt")
    End Function
    'WriteData
    Public Function functionWrite(ByVal objName, ByVal text)
        Dim writetext As System.IO.StreamWriter
        writetext = My.Computer.FileSystem.OpenTextFileWriter(Application.StartupPath & "\" & objName & ".txt", False)
        writetext.Write(text)
        writetext.Close()
        Return writetext
    End Function
End Class
